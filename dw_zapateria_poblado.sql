--poblado tabla dimension clientes
INSERT INTO dim_clientes (keyCliente,nombrecliente,nombrepais,sigla)VALUES
(1,	'BRYAN AUGUSTO CRUZ CASTRO','BOLIVIA','BOL'),
(2,	'JUAN FERNADO ROJAS','BOLIVIA','BOL'),
(3,	'ANTONI PAREDEZ CANDIA','CHILE','CHI'),
(4,'EVO MORALES AYMA','VENEZUELA','VEN'),
(5,'ROBERTO CARLOS','COLOMBIA','COL'),
(6,'ADRIANA MORALES','PERU','PER');

--poblado tabla dimension productos
INSERT INTO dim_productos(keyProducto,nombreproducto,nombrepresentacion,sigla)VALUES
(1,	'BAMBAS','CUERO REVUELTO','CEOREV'),
(2,	'CONVERS','CUERO','CEO'),
(3,	'MANOLETINAS','CUERO','CEO'),
(4,	'TACONES','CUERO','CEO'),
(5,	'TACONES','CUERO','CEO'),
(6,	'OXFORS LISOS','CUERO','CEO'),
(7,	'BOTAS','CUERO','CEO'),
(8,	'BOTINES','CUERO','CEO'),
(9,	'ZAPATILLA','CUERO','CEO'),
(10,'MOCASINES','CUERO','CEO'),
(11	,'DERBI','CUERO REVUELTO','CEOREV'),
(12,'MOCASINES DAMAS','CUERO','CEO'),
(13	,'MOCASINES NIÑOS','CUERO','CEO'),
(14,'SANDALIAS VARON','CUERO REVUELTO','CEOREV'),
(15,'SANDALIAS DAMA','CUERO REVUELTO','CEOREV');

--poblado tabla dimension tiempo
INSERT INTO dim_tiempo (keyTiempo,fecha,anio,mes,dia,semana,trimestre,ntrimestre,nmes,ndia)VALUES
    (20200601,'2020-06-01',2020, 6,1 , 23,2,'T2/20','Junio','Monday'),
    (20200602,'2020-06-02',2020, 6,2 , 23,2,'T2/20','Junio','Tuesday'),
    (20200603,'2020-06-03',2020, 6,3 , 23,2,'T2/20','Junio','Wednesday'),
    (20200604,'2020-06-04',2020, 6,4 , 23,2,'T2/20','Junio','Thursday' ),
    (20200605,'2020-06-05',2020, 6,5 , 23,2,'T2/20','Junio','Friday'   ),
    (20200606,'2020-06-06',2020, 6,6 , 23,2,'T2/20','Junio','Saturday' ),
    (20200607,'2020-06-07',2020, 6,7 , 23,2,'T2/20','Junio','Sunday'   ),
    (20200608,'2020-06-08',2020, 6,8 , 24,2,'T2/20','Junio','Monday'   ),
    (20200609,'2020-06-09',2020, 6,9 , 24,2,'T2/20','Junio','Tuesday'  ),
    (20200610,'2020-06-10',2020, 6,10, 24,2,'T2/20','Junio','Wednesday'),
    (20200611,'2020-06-11',2020, 6,11, 24,2,'T2/20','Junio','Thursday' ),
    (20200612,'2020-06-12',2020, 6,12, 24,2,'T2/20','Junio','Friday'   ),
    (20200613,'2020-06-13',2020, 6,13, 24,2,'T2/20','Junio','Saturday' ),
    (20200614,'2020-06-14',2020, 6,14, 24,2,'T2/20','Junio','Sunday'   ),
    (20200615,'2020-06-15',2020, 6,15, 25,2,'T2/20','Junio','Monday'   ),
    (20200616,'2020-06-16',2020, 6,16, 25,2,'T2/20','Junio','Tuesday'  ),
    (20200617,'2020-06-17',2020, 6,17, 25,2,'T2/20','Junio','Wednesday'),
    (20200618,'2020-06-18',2020, 6,18, 25,2,'T2/20','Junio','Thursday' ),
    (20200619,'2020-06-19',2020, 6,19, 25,2,'T2/20','Junio','Friday'   ),
    (20200620,'2020-06-20',2020, 6,20, 25,2,'T2/20','Junio','Saturday' ),
    (20200621,'2020-06-21',2020, 6,21, 25,2,'T2/20','Junio','Sunday'   ),
    (20200622,'2020-06-22',2020, 6,22, 26,2,'T2/20','Junio','Monday'   ),
    (20200623,'2020-06-23',2020, 6,23, 26,2,'T2/20','Junio','Tuesday'  ),
    (20200624,'2020-06-24',2020, 6,24, 26,2,'T2/20','Junio','Wednesday'),
    (20200625,'2020-06-25',2020, 6,25, 26,2,'T2/20','Junio','Thursday' ),
    (20200626,'2020-06-26',2020, 6,26, 26,2,'T2/20','Junio','Friday'   ),
    (20200627,'2020-06-27',2020, 6,27, 26,2,'T2/20','Junio','Saturday' ),
    (20200628,'2020-06-28',2020, 6,28, 26,2,'T2/20','Junio','Sunday'   ),
    (20200629,'2020-06-29',2020, 6,29, 27,2,'T2/20','Junio','Monday'   ),
    (20200630,'2020-06-30',2020, 6,30, 27,2,'T2/20','Junio','Tuesday'  );

--poblado tabla fact factura - historial trasanccional de facturas
INSERT INTO fact_factura (id_factfactura,keytiempo,nro_factura,keycliente,keyproducto,cantidad,valor_pago,venta)VALUES
    (1,     20200606,5	,1,	15,	10	,28200	,1),
    (2,     20200606,6	,1,	10,	20	,14400	,1),
    (3,     20200606,7	,1,	5,	30	,10290	,1),
    (1,	    20200608,8	,6,	15,	5	,1410	,1),
    (2,	    20200608,9	,6,	14,	15	,3000	,1),
    (3,	    20200608,10	,4,	1,	10	,1256.5	,1),
    (4,	    20200608,11	,4,	2,	10	,2500	,1),
    (5,	    20200608,12	,4,	3,	10	,2000	,1),
    (6,	    20200608,13	,4,	4,	10	,3500	,1),
    (7,	    20200608,14	,2,	15,	20	,5640	,1),
    (1,	    20200610,15	,1,	15,	10	,28200	,1),
    (2,	    20200610,16	,1,	10,	20	,14400	,1),
    (3,	    20200610,17	,1,	5,	30	,10290	,1),
    (1,	    20200611,18	,5,	6,	15	,1256.5	,1),
    (2,	    20200611,19	,5,	7,	15	,2500	,1),
    (3,	    20200611,20	,5,	8,	15	,2000	,1),
    (4,	    20200611,21	,5,	9,	15	,3500	,1),
    (1,	    20200615,22	,2,	1,	10	,1256.5	,1),
    (2,	    20200615,23	,2,	2,	10	,2500	,1),
    (3,	    20200615,24	,2,	3,	10	,2000	,1),
    (4,	    20200615,25	,2,	4,	10	,3500	,1),
    (5,	    20200615,26	,2,	6,	10	,1256.5	,1),
    (6,	    20200615,27	,2,	7,	10	,2500	,1),
    (7,	    20200615,28	,2,	8,	10	,2000	,1),
    (8,	    20200615,29	,2,	9,	10	,3500	,1),
    (1,	    20200618,30	,6,	1,	10	,1256.5	,1),
    (2,	    20200618,31	,6,	2,	10	,2500	,1),
    (3,	    20200618,32	,6,	3,	10	,2000	,1),
    (4,	    20200618,33	,6,	4,	10	,3500	,1),
    (5,	    20200618,34	,6,	6,	10	,1256.5	,1),
    (6,	    20200618,35	,6,	7,	10	,2500	,1),
    (7,	    20200618,36	,6,	8,	10	,2000	,1),
    (8,	    20200618,37	,6,	9,	10	,3500	,1),
    (1,	    20200619,38	,2,	1,	10	,1256.5	,1),
    (2,	    20200619,39	,2,	2,	10	,2500	,1),
    (3,	    20200619,40	,2,	3,	10	,2000	,1),
    (4,	    20200619,41	,2,	4,	10	,3500	,1),
    (5,	    20200619,42	,2,	6,	10	,1256.5	,1),
    (6,	    20200619,43	,2,	7,	10	,2500	,1),
    (7,	    20200619,44	,2,	8,	10	,2000	,1),
    (8,	    20200619,45	,2,	9,	10	,3500	,1),
    (1,	    20200621,46	,1,	1,	10	,1256.5	,1),
    (2,	    20200621,47	,1,	2,	10	,2500	,1),
    (3,	    20200621,48	,1,	3,	10	,2000	,1),
    (4,	    20200621,49	,1,	4,	10	,3500	,1),
    (5,	    20200621,50	,1,	6,	10	,1256.5	,1),
    (6,	    20200621,51	,1,	7,	10	,2500	,1),
    (7,	    20200621,52	,1,	8,	10	,2000	,1),
    (8,	    20200621,53	,1,	9,	10	,3500	,1),
    (9,	    20200621,54	,5,	1,	10	,1256.5	,1),
    (10,	20200621,55	,5,	2,	10	,2500	,1),
    (11,	20200621,56	,5,	3,	10	,2000	,1),
    (12,	20200621,57	,5,	4,	10	,3500	,1),
    (13,	20200621,58	,5,	6,	10	,1256.5	,1),
    (14,	20200621,59	,5,	7,	10	,2500	,1),
    (15,	20200621,60	,5,	8,	10	,2000	,1),
    (16,	20200621,61	,5,	9,	10	,3500	,1),
    (1,	    20200622,62	,3,	1,	10	,1256.5	,1),
    (2,	    20200622,63	,3,	2,	10	,2500	,1),
    (3,	    20200622,64	,3,	3,	10	,2000	,1),
    (4,	    20200622,65	,3,	4,	10	,3500	,1),
    (5,	    20200622,66	,3,	6,	10	,1256.5	,1),
    (6,	    20200622,67	,3,	7,	10	,2500	,1),
    (7,	    20200622,68	,3,	8,	10	,2000	,1),
    (8,	    20200622,69	,3,	9,	10	,3500	,1),
    (1,	    20200623,70	,4,	1,	10	,1256.5	,1),
    (2,	    20200623,71	,4,	2,	10	,2500	,1),
    (3,	    20200623,72	,4,	3,	10	,2000	,1),
    (4,	    20200623,73	,4,	4,	10	,3500	,1),
    (5,	    20200623,74	,4,	6,	10	,1256.5	,1),
    (6,	    20200623,75	,4,	7,	10	,2500	,1),
    (7,	    20200623,76	,4,	8,	10	,2000	,1),
    (8,	    20200623,77	,4,	9,	10	,3500	,1);