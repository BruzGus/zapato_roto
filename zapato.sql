CREATE DATABASE zapato;

--Creacion de la tabla PRESENTACIONES
-- Esta tabla es la que alojara las diferentes presentaciones de nuestros productos
CREATE TABLE presentaciones(
    id smallserial,
    sigla CHARACTER VARYING(10) ,
    nombre CHARACTER VARYING(30) not null,
    CONSTRAINT pk_presentaciones PRIMARY KEY(id)
);

-- Creacion de la tabla PRODUCTOS
--tabla para alojar el catalogo de nuestros productos.
CREATE TABLE productos(
    id  smallserial,
    nombre CHARACTER VARYING(30),
    valor FLOAT,
    id_presentacion SMALLINT,
    CONSTRAINT pk_productos PRIMARY KEY (id),
    CONSTRAINT fk_productos_presentaciones FOREIGN key(id_presentacion) REFERENCES presentaciones(id)
);

--Creacion de la tabla INVENTARIOS
-- tabla para alojar informacion de todo inventario interno de la empresa
CREATE TABLE inventario(
    id  smallserial,
    tipo CHARACTER VARYING(3),
    fecha TIMESTAMP,
    cantidad INTEGER,
    id_producto SMALLINT,
    CONSTRAINT pk_inventario PRIMARY KEY(id),
    CONSTRAINT fk_inventario_productos FOREIGN key(id_producto) REFERENCES productos(id)
);

ALTER TABLE inventario alter COLUMN fecha SET DEFAULT CURRENT_TIMESTAMP;


--Creacion de la tabla DESCUENTOS
--tabla que alojara los diferentes descuentos por periodo de un producto especifico.
CREATE TABLE descuentos(
    id smallserial,
    fecha_inicio DATE,
    fecha_final DATE,
    porcentaje INTEGER,
    id_producto SMALLINT,
    CONSTRAINT pk_descuentos PRIMARY KEY(id),
    CONSTRAINT fk_descuentos_productos FOREIGN key(id_producto) REFERENCES productos(id) 
);

ALTER TABLE descuentos ALTER COLUMN fecha_inicio SET DEFAULT CURRENT_DATE;

--Creacion de tabla PAISES
-- tabla para el catalogo de los diferentes pais de recidencia de nuestros clientes.
CREATE TABLE paises(
    id smallserial,
    sigla CHARACTER VARYING(3),
    nombre CHARACTER VARYING(20),
    CONSTRAINT pk_paises PRIMARY KEY(id)
);

--Creacion de la tabla CLIENTES
-- Tabla que aloja las caracteristicas principales de los clientes de la empresa.
CREATE TABLE clientes(
    id smallserial,
    nombre CHARACTER VARYING(50),
    id_pais SMALLINT,
    CONSTRAINT pk_clientes PRIMARY KEY(id),
    CONSTRAINT fk_clientes_paises FOREIGN KEY(id_pais) REFERENCES paises(id)
);

--Creacion de la tabla FACTURACION
-- Tabla transaccional que alojara la da una factura emitida a nuestro clientes.
CREATE TABLE facturacion(
    nro smallserial,
    fecha TIMESTAMP,
    id_cliente SMALLINT,
    id_producto SMALLINT,
    cantidad INTEGER,
    descuento INTEGER,
    valor_pago FLOAT,
    CONSTRAINT pk_facturacion PRIMARY KEY(nro),
    CONSTRAINT fk_facturacion_clientes FOREIGN key(id_cliente) REFERENCES clientes(id),
    CONSTRAINT fk_facturacion_productos FOREIGN key(id_producto) REFERENCES productos(id)    
);

ALTER TABLE facturacion ALTER COLUMN fecha set DEFAULT CURRENT_TIMESTAMP;