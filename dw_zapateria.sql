/*DATA WAREHOUSE ZAPATERIA "ZAPATO ROTO"*/


--CREACION DEL REPOSITORIO DE DATOS

CREATE DATABASE zapatoDW

--CREACION DE LA DIMENSION PRODUCTOS.
--tabla para alojar los productos
CREATE TABLE dim_productos(
    keyProducto int,
    nombreproducto CHARACTER VARYING(30),
    nombrepresentacion CHARACTER VARYING(30),
    sigla CHARACTER VARYING(10),
    CONSTRAINT pk_dim_productos PRIMARY KEY(keyProducto)
);

--CREACION DE LA DIMENSION CLIENTES
--tabla para alojar los clientes de la empresa
CREATE TABLE dim_clientes(
    keyCliente int,
    nombrecliente CHARACTER VARYING(50),
    nombrepais CHARACTER VARYING(20),
    sigla CHARACTER VARYING(3),
    CONSTRAINT pk_dim_clientes PRIMARY KEY(keyCliente)
);

--CREACION DE LA DIMENTION TIEMPO
--tabla que aloja los distintos periodos de tiempo
CREATE TABLE dim_tiempo(
    keyTiempo int,
    fecha DATE,
    anio SMALLINT,
    mes SMALLINT,
    dia SMALLINT,
    semana SMALLINT,
    trimestre SMALLINT,
    ntrimestre CHARACTER VARYING(7),
    nmes CHARACTER VARYING(15),
    ndia CHARACTER VARYING(10),
    CONSTRAINT pk_dim_tiempo PRIMARY KEY(keyTiempo)
);

--CREACION DEL HISTORIAL DE LA FACTURA
--Tabla que alojara data sobre el historio de la facturas.
CREATE TABLE fact_factura(
    id_factfactura int,
    keyTiempo int,
    nro_factura int,
    keyCliente int,
    keyProducto int,
    cantidad int,
    valor_pago float,
    venta int,
    CONSTRAINT pk_fact_factura PRIMARY KEY(id_factfactura,keyTiempo),
    CONSTRAINT fk_factfactura_tiempo FOREIGN key(keyTiempo) REFERENCES dim_tiempo(keyTiempo),
    CONSTRAINT fk_factfactura_cliente FOREIGN key(keyCliente) REFERENCES dim_clientes(keyCliente),
    CONSTRAINT fk_factfactura_producto FOREIGN key(keyProducto) REFERENCES dim_productos(keyProducto)
);
