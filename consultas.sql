/*CONSULTAS PARA LA ZAPATERIA EL "ZAPATO ROTO"*/
--1.- Consulta la facturación de un cliente en específico.
SELECT a.nro as numero_factura
,a.fecha AS fechaEmision
,b.nombre AS cliente
,a.valor_pago
FROM facturacion a INNER JOIN clientes b 
                    ON(a.id_cliente = b.id)
WHERE b.nombre='BRYAN AUGUSTO CRUZ CASTRO'
ORDER BY 2;

--2.- Consulta la facturación de un producto en específico.
SELECT 
a.nro AS numero_factura
,a.fecha AS fecha_emision
,c.nombre AS clientes
,a.cantidad as cantidad
,b.nombre AS producto
,d.nombre AS nombrePresentacion
,a.valor_pago as total_a_pagar 
FROM facturacion a INNER JOIN productos b 
                        ON(a.id_producto = b.id)
                   INNER JOIN clientes c
                        ON(a.id_cliente = c.id)
                   INNER JOIN presentaciones d
                        ON(b.id_presentacion = d.id)     
WHERE b.nombre ='MOCASINES';

-- 3.- Consulta la facturación de un rango de fechas.

SELECT * FROM facturacion where fecha BETWEEN '2020/06/06' AND '2020/06/18';


-- 4.- Consulta la facturación de un rango de fechas.
SELECT 
a.id_cliente AS codigoCliente
,b.nombre as Cliente
,count(*) as NroCompras
FROM facturacion a INNER JOIN clientes b 
                    ON(a.id_cliente = b.id)
GROUP BY a.id_cliente,b.nombre
ORDER BY 1;